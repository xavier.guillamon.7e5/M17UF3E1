using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CoinSpawner : MonoBehaviour
{
    [SerializeField] GameObject _coinPrefab;
    void Start()
    {
        Instantiate(_coinPrefab, new Vector3(-10.41f, 2.57f, 6.14f), new Quaternion(0, 0, 0, 0));
        Instantiate(_coinPrefab, new Vector3(-7f, 3.74f, 1.54f), new Quaternion(0, 0, 0, 0));
        Instantiate(_coinPrefab, new Vector3(-9.76f, 1.49f, 11.14f), new Quaternion(0, 0, 0, 0));
        Instantiate(_coinPrefab, new Vector3(-1.66f, 4.64f, 14.58f), new Quaternion(0, 0, 0, 0));
        Instantiate(_coinPrefab, new Vector3(3.95f, 4.64f, 11.35f), new Quaternion(0, 0, 0, 0));
        Instantiate(_coinPrefab, new Vector3(6.87f, 4.73f, 10.13f), new Quaternion(0, 0, 0, 0));
        Instantiate(_coinPrefab, new Vector3(4.92f, 6.83f, 9.25f), new Quaternion(0, 0, 0, 0));
        Instantiate(_coinPrefab, new Vector3(10.381f, 7.627f, 7.27f), new Quaternion(0, 0, 0, 0));
        Instantiate(_coinPrefab, new Vector3(18.51f, 1.51f, 3.08f), new Quaternion(0, 0, 0, 0));
        Instantiate(_coinPrefab, new Vector3(10.3f, 1.65f, 0.13f), new Quaternion(0, 0, 0, 0));
        Instantiate(_coinPrefab, new Vector3(-8.02f, 2.645f, 25.525f), new Quaternion(0, 0, 0, 0));
        Instantiate(_coinPrefab, new Vector3(-3.623f, 1.531f, 26.281f), new Quaternion(0, 0, 0, 0));
    }


}
