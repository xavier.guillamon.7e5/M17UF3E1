using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "New Coin", menuName = "Coin Data")]

public class CoinData : ScriptableObject
{
    public float points;
}
