using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu(fileName = "New Inventory", menuName = "Inventory")]
public class InventoryObject : ScriptableObject
{
    public List<InventorySlot> inventory = new List<InventorySlot>();
    public void AddCoin(int _amount)
    {
        inventory.Add(new InventorySlot(_amount));
    }
    public void AddAmountCoins()
    {
        inventory[0].AddAmount(1);
        if (inventory[0].amount >= 12) SceneManager.LoadScene("Playground");
    }
}

[System.Serializable]
public class InventorySlot
{
    public int amount;
    public InventorySlot(int _amount)
    {
        amount = _amount;
    }

    public void AddAmount(int value)
    {
        amount += value;
    }
}
